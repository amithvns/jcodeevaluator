package com.javarunner;

/**
 * Created by Amith on 9/18/2015.
 */

import java.io.File;
import java.io.IOException;

public class Shell {

    private ProcessBuilder pb;
    private StringBuilder output = new StringBuilder();

    public Shell(String inputPath, String shellFile) {
        pb = new ProcessBuilder(inputPath + "\\" + shellFile + ".bat");
        pb.directory(new File(inputPath));
    }

    public String run() {
        try {
            Process process = pb.start();
            IOThreadHandler outputHandler = new IOThreadHandler(
                    process.getInputStream());
            outputHandler.start();
            process.waitFor();
            output = outputHandler.getOutput();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return output.toString();
    }
}