package com.javarunner;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    final public String FOLDER_NAME = "submissions";
    final public String INPUT_FOLDER = "D:\\Masters\\Fall 2015\\CPS 180\\NeedsGrading\\" + FOLDER_NAME;
    final public String INPUT_FILEPATH = "D:\\Masters\\Fall 2015\\CPS 180\\NeedsGrading\\";
    private List<File> mainClasses = new ArrayList<>();

    public static void main(String[] args) {
        new Main().organize();
        //new Main().evaluate();
    }

    public void organize() {
        new OrganizeFiles(INPUT_FOLDER);
    }

    public void evaluate() {
        List<String> inputFiles = new ArrayList<>();
        inputFiles.add("input.txt");
        File input = new File(INPUT_FOLDER);
        File[] folders = input.listFiles();
        if (folders != null) {
            for (File folder : folders) {
                if (folder.isDirectory() && !folder.getName().contains("-c") && !folder.getName().contains("-error")) {
                    mainClasses = new ArrayList<>();
                    System.out.println(folder.getName());
                    initiateFiles(folder, inputFiles);
                    compileFiles(folder);
                    executeFiles(folder, inputFiles);
                }
            }
        }
        markFolders(folders);
    }

    private void initiateFiles(File folder, List<String> inputFiles) {
        //Identifying java files with main method
        prepareJavaFiles(folder);
        try {
            new OrganizeFiles().copyFiles(INPUT_FILEPATH, folder.getAbsolutePath(), inputFiles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void compileFiles(File folder) {
        //Compiling all java files
        List<String> statements = createCompilerStatements(folder);
        createBatchFile(folder, statements, "compile");
        Shell compiler = new Shell(folder.getAbsolutePath(), "compile");
        compiler.run();
    }

    private void executeFiles(File folder, List<String> inputFiles) {
        //Execute all java files with main method
        for (File javaFile : mainClasses)
            if (inputFiles.size() != 0)
                for (String inputFile : inputFiles)
                    executeFile(folder, javaFile, inputFile);
            else
                executeFile(folder, javaFile, "");
    }

    private void executeFile(File folder, File javaFile, String inputFile) {
        List<String> statements;
        if (inputFile.equals(""))
            statements = createShellStatements(javaFile);
        else
            statements = createShellStatements(javaFile, inputFile);
        createBatchFile(folder, statements, "evaluate" + fileNameWithoutExt(javaFile.getName()));
        Shell exec = new Shell(folder.getAbsolutePath(), "evaluate" + fileNameWithoutExt(javaFile.getName()));
        exec.run();
        try {
            File temp = new File(folder.getAbsolutePath() + "\\temp.txt");
            Scanner out = new Scanner(temp).useDelimiter("\\Z");
            String output = "";
            if (out.hasNext())
                output = out.next();
            appendToFile(javaFile, output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void markFolders(File[] folders) {
        for (File folder : folders) {
            if (folder.isDirectory() && (!folder.getName().contains("-error") || !folder.getName().contains("-c"))) {
                if (!new File(folder.getAbsolutePath() + "\\temp.txt").exists()) {
                    File compiledFolder = new File(folder.getAbsolutePath() + "-error");
                    folder.renameTo(compiledFolder);
                } else if (!folder.getName().contains("-c")) {
                    File compiledFolder = new File(folder.getAbsolutePath() + "-c");
                    folder.renameTo(compiledFolder);
                }
            }
        }
    }

    private void prepareJavaFiles(File folder) {
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (isJavaFile(file)) {
                    try {
                        Scanner scanner = new Scanner(file);
                        while (scanner.hasNextLine()) {
                            String line = scanner.nextLine();
                            if (line != null && checkForMain(line)) {
                                mainClasses.add(file);
                            }
                        }
                        scanner.close();
                        removePackageInfo(file);
                        // setFilePaths(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public boolean checkForMain(String line) {
        return line.contains("public") && line.contains("static") &&
                line.contains("void") && line.contains("main") &&
                line.contains("String") && line.contains("[]");
    }

    public void removePackageInfo(File file) throws IOException {
        File tmp = File.createTempFile(file + "tmp", "");
        BufferedReader br = new BufferedReader(new FileReader(file));
        BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));
        String line;
        while (null != (line = br.readLine())) {
            if (!line.contains("package "))
                bw.write(String.format("%s%n", line));
        }
        br.close();
        bw.close();
        if (file.delete())
            tmp.renameTo(file);

    }

    public void setFilePaths(File file) throws IOException {
        File tmp = File.createTempFile(file + "tmp", "");
        BufferedReader br = new BufferedReader(new FileReader(file));
        BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));
        String line;
        while (null != (line = br.readLine())) {
            if (!line.contains("new File")) {
                bw.write(String.format("%s%n", line));
            } else {
                String newLine = line;
                if (line.contains(".txt")) {
                    newLine = line.substring(0, line.indexOf('"')) + "\"grades.txt\");";
                } else if (line.contains("friendInput.txt")) {
                    newLine = line.substring(0, line.indexOf('"')) + "\"friendInput.txt\");";
                }
                bw.write(String.format("%s%n", newLine));
            }
        }
        br.close();
        bw.close();
        if (file.delete())
            tmp.renameTo(file);
    }

    private boolean isJavaFile(File file) {
        if (file.isFile() && file.getName().contains(".java"))
            return true;
        else
            return false;
    }

    private void appendToFile(File javaFile, String append) {
        try {
            FileWriter fileWritter = new FileWriter(javaFile, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write("/* \n");
            bufferWritter.write("Output \n");
            bufferWritter.write(append);
            bufferWritter.write("*/ \n");
            bufferWritter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> createCompilerStatements(File folder) {
        List<String> statements = new ArrayList<>();
        File[] files = folder.listFiles();
        statements.add("@echo off");
        if (files != null) {
            for (File file : files) {
                if (file.isFile() && file.getName().contains(".java")) {
                    statements.add("javac " + file.getName());
                }
            }
        }
        return statements;
    }

    private String fileNameWithoutExt(String name) {
        int pos = name.lastIndexOf(".");
        if (pos > 0) {
            return name.substring(0, pos);
        }
        return "";
    }

    private List<String> createShellStatements(File file) {
        List<String> statements = new ArrayList<>();
        statements.add("@echo off");
        statements.add("java " + fileNameWithoutExt(file.getName()) + " > temp.txt");
        return statements;
    }

    private List<String> createShellStatements(File file, String input) {
        List<String> statements = new ArrayList<>();
        statements.add("@echo off");
        statements.add("java " + fileNameWithoutExt(file.getName()) + " < " + input + " > temp.txt");
        return statements;
    }

    private void createBatchFile(File folder, List<String> statements, String shellFile) {
        String batchFilePath = folder.getAbsolutePath() + "\\" + shellFile + ".bat";
        try {
            File file = new File(batchFilePath);
            file.createNewFile();
            PrintWriter writer = new PrintWriter(file, "UTF-8");
            statements.forEach(writer::println);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
