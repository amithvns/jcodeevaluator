package com.javarunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Amith on 9/19/2015.
 */
public class OrganizeFiles {

    private static String INPUT_PATH = "";

    public OrganizeFiles() {

    }

    public OrganizeFiles(String inputPath) {
        INPUT_PATH = inputPath;
        File input = new File(INPUT_PATH);
        File[] files = input.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                String str = file.getName();
                String[] parts = str.split("_");
                if (parts.length > 3) {
                    File folder = new File(INPUT_PATH + "\\" + parts[1]);
                    if (!folder.exists())
                        folder.mkdir();
                    String fileName = "";
                    if (!file.getName().contains(".java")) {
                        fileName = parts[2] + parts[3];
                    } else {
                        for (int i = 4; i < parts.length; i++) {
                            fileName = (i > 4) ? fileName + "_" : fileName;
                            fileName = fileName + parts[i];
                        }
                    }
                    moveFile(file.getAbsolutePath(), folder.getAbsolutePath() + "\\" + fileName);
                }
            }
        }
    }

    public void moveFile(String source, String target) {
        File sourceFolder = new File(source);
        File targetFolder = new File(target);
        sourceFolder.renameTo(targetFolder);
    }

    public void copyFile(String source, String target) throws IOException {
        File sourceFolder = new File(source);
        File targetFolder = new File(target);
        if (!targetFolder.exists()) {
            Files.copy(sourceFolder.toPath(), targetFolder.toPath());
        }
    }

    public void copyFiles(String source, String target, List<String> files) throws IOException {
        for (String file : files) {
            copyFile(source + "\\" + file, target + "\\" + file);
        }
    }

    public void renameFile(String original, String renameTo) {
        File oldName = new File(original);
        File newName = new File(renameTo);
        oldName.renameTo(newName);
    }

    public String getExtension(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
        if (i > p) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }
}
